## Directory categorizer

If you work on a computer all day, you may want to ensure your files are organized in a 
manner that will make it easy for you to locate an item quickly. Be specific in terms of 
file names so you can conduct an easy search for a specific item when in a rush. This might 
not seem like a big deal, but it can be a huge time-saver. So, if you are having trouble in 
organizing your files, for example your files in Download directory, then file organizer is 
for you and it will help you categorize your messy directory automatically.

![file_categorizer](https://user-images.githubusercontent.com/48658768/77701097-2c8b0880-6fbe-11ea-8fe1-bc8d2d0be96c.gif)

### Get organized today!