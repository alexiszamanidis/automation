directories =   {
                    "documents" : [ ".oxps", ".epub", ".pages", ".docx", ".doc", ".fdf", ".ods", ".odt", ".pwi", ".xsn", ".xps",
                                    ".dotx", ".docm",".dox", ".rvg", ".rtf", ".rtfd", ".wpd", ".xls", ".xlsx", ".ppt", ".pptx",
                                    ".xml"],
                    "music"     : [ ".aac", ".aa", ".aac", ".dvf", ".m4a", ".m4b", ".m4p", ".mp3", ".msv", "ogg", "oga", ".raw", 
                                    ".vox", ".wav", ".wma" ],
                    "pictures"  : [ ".jpeg", ".jpg", ".tiff", ".gif", ".bmp", ".png", ".bpg", "svg", ".heif", ".psd" ],
                    "videos"    : [ ".avi", ".flv", ".wmv", ".mov", ".mp4", ".webm", ".vob", ".mng", ".qt", ".mpg", ".mpeg", ".3gp" ],
                    "archives"  : [ ".a", ".ar", ".cpio", ".iso", ".tar", ".gz", ".rz", ".7z", ".dmg", ".rar", ".xar", ".zip" ],
                    "pdf"       : [ ".pdf" ],
                    "c"         : [ ".c", ".cpp", ".h", ".hpp" ],
                    "python"    : [ ".py" ],
                    "exe"       : [ ".exe" ],
                    "shell"     : [ ".sh" ],
                    "other"     : [ ]
                } 