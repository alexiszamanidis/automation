import os
import shutil
from directories import directories

# make a dictionary of extesion to directory
extension_to_directory = {
                            extension: directory
                            for directory, extensions in directories.items() 
                            for extension in extensions
                         }

class categorizer:
    def __init__(self):
        self.path = os.getcwd()
        self.files = os.listdir()

    """
    Arguments:
        directory_name: string
    """
    def create_directory(self, directory_name):
        if not os.path.exists(directory_name):
            os.mkdir(directory_name)

    """
    Arguments:
        directory: string
        file: string
    """
    def create_print_move(self, directory, file):
        if file != os.path.basename(__file__):
            self.create_directory(directory)
            filename, file_extension = os.path.splitext(file)
            i = 0
            while True:
                new_filename = (f"{file}" if ( i == 0 ) else "{filename}({i}){file_extension}")
                if not os.path.exists(f"./{directory}/{new_filename}"):
                    break
                i = i + 1
            print(f"{file} -> {directory}/{new_filename}")
            shutil.move(file,f"./{directory}/{new_filename}")

    """
    Arguments:
        file: string
    """
    def categorize_file(self, file):
        extension = os.path.splitext(file)[1]
        if extension in extension_to_directory:
            directory = extension_to_directory[extension]
            self.create_print_move(directory,file)
        elif( ((os.path.isdir(file)) and (file not in directories)) or (not os.path.isdir(file)) ):
            self.create_print_move("other",file)

    """
    Categorize all files and directories from current path directory
    """
    def categorize_directory(self):
        for file in self.files:
            self.categorize_file(file)

if __name__ == "__main__":
    enter = input("Press 'Enter' to organize your directory: ")
    if enter == "" :
        categorizer = categorizer()
        categorizer.categorize_directory()
    else:
        print("Stay with junk!")
    input("Press any key to exit..") 