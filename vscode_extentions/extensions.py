extensions = [
    "abusaidm.html-snippets",
    "Atishay-Jain.All-Autocomplete",
    "CoenraadS.bracket-pair-colorizer",
    "dbaeumer.vscode-eslint",
    "dsznajder.es7-react-js-snippets",
    "eamodio.gitlens",
    "ecmel.vscode-html-css",
    "esbenp.prettier-vscode",
    "formulahendry.auto-close-tag",
    "formulahendry.auto-rename-tag",
    "GabrielBB.vscode-lombok",
    "hwencc.html-tag-wrapper",
    "ionutvmi.path-autocomplete",
    "jolaleye.horizon-theme-vscode",
    "ms-azuretools.vscode-docker",
    "ms-python.python",
    "ms-toolsai.jupyter",
    "ms-vscode-remote.remote-wsl",
    "Pivotal.vscode-boot-dev-pack",
    "Pivotal.vscode-concourse",
    "Pivotal.vscode-manifest-yaml",
    "Pivotal.vscode-spring-boot",
    "PKief.material-icon-theme",
    "pranaygp.vscode-css-peek",
    "redhat.java",
    "ritwickdey.LiveServer",
    "steoates.autoimport",
    "vscjava.vscode-java-debug",
    "vscjava.vscode-java-dependency",
    "vscjava.vscode-java-pack",
    "vscjava.vscode-java-test",
    "vscjava.vscode-maven",
    "vscjava.vscode-spring-boot-dashboard",
    "vscjava.vscode-spring-initializr",
    "yzane.markdown-pdf",
    "yzhang.markdown-all-in-one",
    "Zignd.html-css-class-completion"
]