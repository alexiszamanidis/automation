## Wallpaper

A script that uses Unsplash API to find a Full HD image for your desktop wallpaper.

### How to run the script

1. Generate you own Unsplash access key
2. Replace the access_key = "YOUR_UNSPLASH_ACCESS_KEY" in secret.py file with your own key
3. Run the script: `python wallpaper.py`

The script will download images until you stop it by sending a signal with `Ctrl + C`.

![wallpaper](/uploads/ed13d3a804f7461ea3c85356b3270496/wallpaper.gif)

### Testing

OS: Windows 10

### Unsplash API Documentation

You can read more about [**Unsplash API here**](https://unsplash.com/documentation).
