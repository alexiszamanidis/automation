import os
import time
import wget
import ctypes
import requests
from requests.exceptions import HTTPError
import secret

def print_purple(text): print(f"\033[95m{text}\033[00m")
def print_red(text): print(f"\033[91m{text}\033[00m")

class wallpaper:
    """
    Arguments:
        time: string
    """
    def __init__(self, time, delete = True):
        self.time = time
        self.delete = delete
        self.wallpaper_name = None

    """
    Arguments:
        directory_name: string
    """
    def create_directory(self,directory_name):
        if not os.path.exists(directory_name):
            os.mkdir(directory_name)

    def download_wallpaper(self):
        access_key = secret.access_key
        url = "https://api.unsplash.com/photos/random?client_id=" + access_key
        parameters = {
                        "query": "HD wallpapers",       # limit selection to photos matching a search term.
                        "orientation": "landscape"      # filter by photo orientation. (Valid values: landscape, portrait, squarish)
                     }
        try:
            response = requests.get(url,params=parameters).json()
            wallpaper_source = response["urls"]["full"]
        except HTTPError as http_error:
            print_red(f"HTTP error occurred: {http_error}")
            return None
        except Exception as error:
            print_red(f"Error occurred: {error}")
            return None

        self.create_directory("wallpapers")

        # create_unique_name
        i = 0
        while True:
            self.wallpaper_name = (("./wallpapers/wallpaper.jpg") if ( i == 0 ) else (f"./wallpapers/wallpaper({i}).jpg"))
            if not os.path.exists(self.wallpaper_name):
                break
            i += 1

        # download wallpaper
        wget.download(wallpaper_source, self.wallpaper_name)

    def change_wallpaper(self):
        # download wallpaper and get wallpaper name
        self.download_wallpaper()
        if self.wallpaper_name is not None:
            # set new wallpaper for windows
            ctypes.windll.user32.SystemParametersInfoW(20,0,f"{os.getcwd()}/{self.wallpaper_name}",3)

    def delete_wallpapers(self):
        path = f"{os.getcwd()}/wallpapers"
        # get user's chosen wallpaper name
        self.wallpaper_name = self.wallpaper_name.split("/")[2]
        # get a list of all wallpapers that were downloaded
        wallpapers = os.listdir(path)
        # delete all wallpapers except the chosen one
        for wallpaper in wallpapers:
            if wallpaper != self.wallpaper_name:
                os.remove(f"{path}/{wallpaper}")
        # rename chosen wallpaper to 'wallpaper.jpg'
        os.rename(f"{path}/{self.wallpaper_name}",f"{path}/wallpaper.jpg")

    def search_wallpaper(self):
        try:
            while True:
                self.change_wallpaper()
                time.sleep(self.time)
        except KeyboardInterrupt:
            if self.delete == True:
                self.delete_wallpapers()
            print_purple("\nHope you found the image that you were looking for!")

if __name__ == "__main__":
    wallpaper = wallpaper(5)
    wallpaper.search_wallpaper()